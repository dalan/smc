#!/usr/bin/env -S just --justfile

alias b := build

build:
  cargo build

build_linux_musl:
  cargo build --target=x86_64-unknown-linux-musl --release

build_win_gnu:
  cargo build --target=x86_64-pc-windows-gnu --release

clean_dist:
  rm -Rf ./dist

dist: clean_dist build_linux_musl build_win_gnu
  mkdir dist
  cargo run -r -- bin_win files 
  cargo run -r -- bin_linux files 
  cargo run -r -- sources