use crate::backup;
use anyhow::{bail, Result};
use chrono::{DateTime, Local, NaiveDateTime};
use clap::ArgMatches;
use gix::bstr::ByteSlice;
use serde::{Deserialize, Serialize};
use std::{
    env::{current_dir, set_current_dir},
    path::PathBuf,
};
use strum_macros::{Display, EnumString, VariantNames};

#[derive(Copy, Clone, Debug, EnumString, Deserialize, VariantNames, Display)]
#[strum(ascii_case_insensitive)]
pub enum ContainerType {
    None,
    Zip,
    SevenZip,
    Tar,
}

impl ContainerType {
    pub const fn extension(self, compression: CompressionType) -> &'static str {
        match self {
            ContainerType::None => "",
            ContainerType::Zip => ".zip",
            ContainerType::SevenZip => ".7z",
            ContainerType::Tar => match compression {
                CompressionType::None => ".tar",
                CompressionType::Deflate => ".tar",
                CompressionType::Bzip2 => ".tar.bz2",
                CompressionType::Bzip3 => ".tar.bz3",
                CompressionType::Zstd => ".tar.zst",
                CompressionType::Gzip => ".tar.gz",
                CompressionType::Xz => ".tar.xz",
            },
        }
    }
}

#[derive(Copy, Clone, Debug, EnumString, Deserialize, VariantNames, Display)]
#[strum(ascii_case_insensitive)]
pub enum CompressionType {
    None,
    Deflate,
    Bzip2,
    Bzip3,
    Zstd,
    Gzip,
    Xz,
}

impl TryFrom<CompressionType> for zip::CompressionMethod {
    type Error = anyhow::Error;

    fn try_from(value: CompressionType) -> Result<Self> {
        match value {
            CompressionType::None => Ok(zip::CompressionMethod::Stored),
            CompressionType::Deflate => Ok(zip::CompressionMethod::Deflated),
            CompressionType::Bzip2 => Ok(zip::CompressionMethod::Bzip2),
            CompressionType::Bzip3 => bail!("No bzip3 compression in zip"),
            CompressionType::Zstd => Ok(zip::CompressionMethod::Zstd),
            CompressionType::Gzip => bail!("No gzip compression in zip"),
            CompressionType::Xz => bail!("No xz compression in zip"),
        }
    }
}

pub fn create_app() -> clap::Command {
    backup::create_app()
}

#[derive(Debug, Serialize)]
pub struct Commit {
    pub id: String,
    pub full_id: String,
    pub utc_time: NaiveDateTime,
    pub title: String,
    pub body: Option<String>,
    pub author: String,
    pub tag: Option<String>,
}

impl Commit {
    fn current(git_repo: &gix::Repository) -> Result<Self> {
        let commit = git_repo.head_commit()?;

        let id = commit.id();
        let full_id = id.to_string();
        let id = id.shorten()?.to_string();

        let message = commit.message()?;
        let title = message.title.to_string();
        let body = message.body().map(|b| b.to_str_lossy().trim().to_string());

        let git_author = commit.author()?;
        let author = format!("{} ({})", git_author.name, git_author.email);

        let git_time = commit.time()?;
        let utc_time = DateTime::from_timestamp(git_time.seconds, 0)
            .unwrap()
            .naive_utc();

        let tag = commit
            .describe()
            .try_resolve()?
            .map(|res| res.outcome.name.map(|n| n.to_string()))
            .flatten();

        Ok(Commit {
            id,
            full_id,
            utc_time,
            title,
            body,
            author,
            tag,
        })
    }
}

#[derive(Debug, Serialize)]
pub struct Info {
    pub local_date_time: NaiveDateTime,
    pub git_branch: Option<String>,
    pub current_commit: Option<Commit>,
    pub current_dir: String,
}

impl Info {
    fn new(git_repo: &Option<gix::Repository>) -> Result<Self> {
        let local_date_time = Local::now().naive_local();

        let git_branch = git_repo
            .as_ref()
            .map(|repo| match repo.head_ref() {
                Ok(Some(head)) => Ok(head.name().file_name().to_string()),
                Ok(None) => bail!("Cannot get git branch (HEAD detached)"),
                Err(e) => bail!("Cannot get git branch ({})", e),
            })
            .transpose()?;

        let current_commit = if let Some(git_repo) = git_repo {
            Some(Commit::current(git_repo)?)
        } else {
            None
        };

        let current_dir = current_dir()
            .unwrap()
            .file_name()
            .unwrap()
            .to_string_lossy()
            .to_string();

        Ok(Info {
            local_date_time,
            git_branch,
            current_commit,
            current_dir,
        })
    }
}

#[derive(Debug)]
pub enum Command {
    Backup(backup::Config),
}

#[derive(Debug)]
pub struct Config {
    pub info: Info,
    pub verbosity: i32,
    pub command: Command,
}

impl<'a> TryFrom<&ArgMatches> for Config {
    type Error = anyhow::Error;

    fn try_from(matches: &ArgMatches) -> Result<Self> {
        // Set current dir
        if let Some(directory) = matches.get_one::<PathBuf>("directory") {
            set_current_dir(directory)?;
        }

        // Verbosity
        let verbosity = matches.get_count("verbosity") as i32;

        // Open git repo
        let git_repo = gix::open(current_dir()?).ok();

        // Get infos
        let info = Info::new(&git_repo)?;

        // Get command
        let command = Command::Backup(backup::Config::new(matches, &info)?);

        Ok(Config {
            info,
            verbosity,
            command,
        })
    }
}
