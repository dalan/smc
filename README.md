# smc
Tool to save code and other files.

## Install
`cargo install save_my_code`

## Usage
### Config file
Create a config file named `smc.toml`.

```toml
[profiles]
[profiles.toto]             # Profile name
paths = ["./"]              # Path to include
files = ["./"]              # Files to include
output = "/tmp"             # Output dir or filename (default: "./${CURRENT_DIR}_${TIME}${EXT}")
hidden = true               # Save also hidden (default: false)
gitignore = true            # Follow gitignore (default: true)
container = "Zip"           # Container [None, Zip, SevenZip, Tar] (default: Zip)
compression = "Bzip2"       # Compression if possible [None, Deflate, Bzip2, Bzip3, Zstd, Gzip, Xz] (default: Deflate)
compression_level = 9       # Compression level if possible 
directory = "."             # Current directory (default: ".")
signatures = ["Sha256"]     # Signatures to generate [Blake3, Sha256, Sha384, Sha512, Sha3_256, Sha3_384, Sha3_512, Minisign, OpenPGP]
all_signature = ["Sha256"]  # Signatures to generate [Blake3, Sha256, Sha384, Sha512, Sha3_256, Sha3_384, Sha3_512] for all files saved
ignore = ["Cargo.*"]        # Files to ignore
whitelist = [".gitignore"]  # Whitelist files
metadata = true             # Write metadata in the output container (default: false)
destination = "my_test"     # Specify a destination for all files in the output container
```

Variables can be used in `output` and `save_dir` using `$VAR` or `${VAR}`:

| Name                 | Value                 |
| -------------------- | --------------------- |
| CURRENT_DIR          | Current directory     |
| TIME                 | Current date and time |
| SAVE_DIR             | Save directory        |
| TMP                  | Temp directory,       |
| EXT                  | Container extension   |
| GIT_COMMIT_HASH      | Git commit hash       |
| GIT_FULL_COMMIT_HASH | Git full commit hash  |
| GIT_BRANCH           | Git branch            |
| GIT_TAG              | Latest git tag        |

### Run
Run smc with the profiles names as arguments: `smc profile_name`.

Several profiles can be set. In this case all the files will be put in the first profile container.

Some command line arguments can be used to change profile.

Profiles can also be read from stdin with `cat my_profile.toml | smc`