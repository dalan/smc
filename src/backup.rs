use crate::config::{CompressionType, ContainerType, Info};
use crate::signature::{self, Signature};
use anyhow::Result;
use anyhow::{anyhow, Context};
use clap::{value_parser, Arg, ArgAction, ArgMatches, Command};
use ignore::WalkBuilder;
use indicatif::MultiProgress;
use indicatif::{ProgressBar, ProgressIterator, ProgressStyle};
use itertools::Itertools;
use log::{debug, info, warn};
use path_slash::PathExt;
use relative_path::RelativePath;
use relative_path::RelativePathBuf;
use serde::Deserialize;
use std::borrow::Cow;
use std::collections::HashMap;
use std::env::{current_dir, set_current_dir};
use std::fmt::Display;
use std::fs;
use std::io::{stdin, IsTerminal, Read};
use std::path::{Path, PathBuf};
use std::str::FromStr;
use strum::VariantNames;

pub fn create_app() -> Command {
    Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("verbosity")
                .short('v')
                .long("verbose")
                .action(ArgAction::Count)
                .help("Sets the level of verbosity"),
        )
        .arg(
            Arg::new("output")
                .short('o')
                .long("output")
                .value_name("OUT_NAME")
                .help("Output directory or filename")
                .value_parser(value_parser!(PathBuf))
        )
        .arg(
            Arg::new("save_dir")
                .short('y')
                .long("save_dir")
                .value_name("SAVE_DIR")
                .help("Save directory in output")
        )
        .arg(
            Arg::new("profiles-list")
                .short('r')
                .long("profiles-list")
                .action(ArgAction::SetTrue)
                .help("List profiles")
        )
        .arg(
            Arg::new("paths")
                .short('p')
                .long("paths")
                .value_name("PATH")
                .help("Paths to save")
                .action(ArgAction::Append)
        )
        .arg(
            Arg::new("files")
                .short('f')
                .long("files")
                .value_name("FILE")
                .help("Files to save")
                .action(ArgAction::Append)
        )
        .arg(
            Arg::new("container")
                .short('t')
                .long("container")
                .value_name("CONTAINER")
                .help("Container type")
                .value_parser(ContainerType::VARIANTS.to_vec())
        )
        .arg(
            Arg::new("compression")
                .short('c')
                .long("compression")
                .value_name("COMPRESSION")
                .value_parser(CompressionType::VARIANTS.to_vec())
                .help("Compression method")
        )
        .arg(
            Arg::new("compression_level")
                .short('l')
                .long("comp-level")
                .value_name("COMP_LEVEL")
                .help("Compression level")
                .long_help("Deflated: 0 - 9 (default is 6)\nBzip2: 0 - 9 (default is 6)\nZstd: 1 - 21 (default is 3)\nGzip: 0 - 9 (default is 6)\nXz: 0 - 9 (default is 6)")
                .value_parser(clap::value_parser!(i32) )
        )
        .arg(
            Arg::new("hidden")
                .short('e')
                .long("hidden")
                .action(ArgAction::SetTrue)
                .help("Save also hidden file (begin with .)"),
        )
        .arg(
            Arg::new("gitignore")
                .short('g')
                .long("no-gitignore")
                .action(ArgAction::SetTrue)
                .help("Do not follow gitignore rules"),
        )
        .arg(
            Arg::new("metadata")
                .short('m')
                .long("metadata")
                .action(ArgAction::SetTrue)
                .help("Write metadata in backup file"),
        )
        .arg(
            Arg::new("ignore")
                .short('i')
                .long("ignore")
                .value_name("IGNORE")
                .help("gitignore like line")
                .action(clap::ArgAction::Append)
        )
        .arg(
            Arg::new("whitelist")
                .short('w')
                .long("whitelist")
                .value_name("WHITELIST")
                .help("gitignore like whitelist line")
                .action(clap::ArgAction::Append)
        )
        .arg(
            Arg::new("signatures")
                .short('s')
                .long("signature")
                .value_name("SIGN")
                .help("Signatures to generate")
                .value_parser(Signature::VARIANTS.to_vec())
                .action(ArgAction::Append)
        )
        .arg(
            Arg::new("all-signatures")
                .short('a')
                .long("all-signature")
                .value_name("SIGN")
                .help("Signatures to generate for all files")
                .value_parser(Signature::VARIANTS[..7].to_vec())
        )
        .arg(
            Arg::new("minisign_secret_key_path")
                .long("ms-sk-path")
                .value_name("PATH")
                .help("Minisign secret key path")
                .value_parser(value_parser!(PathBuf))
        )
        .arg(
            Arg::new("openpgp_secret_key_path")
                .long("pgp-sk-path")
                .value_name("PATH")
                .help("OpenPGP secret key path")
                .value_parser(value_parser!(PathBuf))
        )
        .arg(
            Arg::new("directory")
                .short('d')
                .long("directory")
                .value_name("DIR")
                .help("Current directory")
                .value_parser(value_parser!(PathBuf))
        )
        .arg(
            Arg::new("destination")
                .short('n')
                .long("destination")
                .value_name("DEST")
                .help("Destination for all file")
                .value_parser(value_parser!(PathBuf))
        )
        .arg(
            Arg::new("profiles")
                .value_name("CONFIG")
                .help("Profiles used found in smc.toml")
                .action(ArgAction::Append),
        )
}

#[derive(Debug, Deserialize)]
struct Profile {
    #[serde(default)]
    paths: Vec<RelativePathBuf>,
    output: Option<PathBuf>,
    compression_level: Option<i32>,
    hidden: Option<bool>,
    gitignore: Option<bool>,
    container: Option<ContainerType>,
    compression: Option<CompressionType>,
    directory: Option<RelativePathBuf>,
    #[serde(default)]
    files: Vec<RelativePathBuf>,
    #[serde(default)]
    signatures: Vec<Signature>,
    all_signature: Option<Signature>,
    #[serde(default)]
    ignore: Vec<String>,
    #[serde(default)]
    whitelist: Vec<String>,
    metadata: Option<bool>,
    destination: Option<RelativePathBuf>,
    save_dir: Option<String>,
}

impl Default for Profile {
    fn default() -> Self {
        Profile {
            paths: vec![],
            output: Some(PathBuf::from(".")),
            compression_level: None,
            hidden: Some(false),
            gitignore: Some(true),
            container: Some(ContainerType::Zip),
            compression: Some(CompressionType::None),
            directory: None,
            files: vec![],
            signatures: vec![],
            all_signature: None,
            ignore: vec![],
            whitelist: vec![],
            metadata: Some(false),
            destination: None,
            save_dir: Some("${CURRENT_DIR}".to_string()),
        }
    }
}

impl Profile {
    fn or_default(mut self) -> Self {
        let default = Self::default();
        if self.output.is_none() {
            self.output = default.output;
        }
        if self.hidden.is_none() {
            self.hidden = default.hidden;
        }
        if self.gitignore.is_none() {
            self.gitignore = default.gitignore;
        }
        if self.container.is_none() {
            self.container = default.container;
        }
        if self.compression.is_none() {
            self.compression = default.compression;
        }
        if self.metadata.is_none() {
            self.metadata = default.metadata;
        }
        if self.save_dir.is_none() {
            self.save_dir = default.save_dir;
        }
        self
    }

    fn add_cmd(mut self, matches: &ArgMatches) -> Result<Self> {
        // Add save filters
        if matches.get_flag("hidden") {
            self.hidden = Some(true);
        }
        if matches.get_flag("gitignore") {
            self.gitignore = Some(false);
        }

        // Add ignore
        if let Some(ignores) = matches.get_many::<String>("ignore") {
            self.ignore.extend(ignores.map(|i| i.clone()));
        }

        // Add whitelist
        if let Some(whitelists) = matches.get_many::<String>("whitelist") {
            self.whitelist.extend(whitelists.map(|w| w.clone()));
        }

        // Add destination
        if let Some(destination) = matches.get_one::<PathBuf>("destination") {
            self.destination = Some(
                RelativePathBuf::from_path(destination)
                    .with_context(|| "Destination must be a relative path")?,
            )
        }

        Ok(self)
    }
}

macro_rules! print_attr {
    ($profile_name:ident, disp: $attr_name:ident ($printed_name:expr), $formatter_name:ident, $first_char:ident) => {
        if let Some($attr_name) = &$profile_name.$attr_name {
            writeln!(
                $formatter_name,
                "{}   ├ {}: {}",
                $first_char, $printed_name, $attr_name
            )?;
        }
    };
    ($profile_name:ident, path: $attr_name:ident ($printed_name:expr), $formatter_name:ident, $first_char:ident) => {
        if let Some($attr_name) = &$profile_name.$attr_name {
            writeln!(
                $formatter_name,
                "{}   ├ {}: {}",
                $first_char,
                $printed_name,
                $attr_name.display()
            )?;
        }
    };
    ($profile_name:ident, disp_list: $attr_name:ident ($printed_name:expr), $formatter_name:ident, $first_char:ident) => {
        if !$profile_name.$attr_name.is_empty() {
            writeln!($formatter_name, "{}   ├ {}", $first_char, $printed_name)?;
            for (pos, val) in $profile_name.$attr_name.iter().with_position() {
                match pos {
                    itertools::Position::First | itertools::Position::Middle => {
                        writeln!($formatter_name, "{}   │ ├ {}", $first_char, val)?
                    }
                    itertools::Position::Last | itertools::Position::Only => {
                        writeln!($formatter_name, "{}   │ └ {}", $first_char, val)?
                    }
                }
            }
        }
    };
}

impl Display for Profile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let first_char = if f.sign_plus() { '│' } else { ' ' };

        print_attr!(self, path: output("output"), f, first_char);
        print_attr!(self, disp: save_dir("save_dir"), f, first_char);
        print_attr!(self, disp: compression("compression"), f, first_char);
        print_attr!(self, disp: container("container"), f, first_char);
        print_attr!(self, disp: compression_level("compression level"), f, first_char);
        print_attr!(self, disp: hidden("hidden"), f, first_char);
        print_attr!(self, disp: gitignore("gitignore"), f, first_char);
        print_attr!(self, disp: metadata("metadata"), f, first_char);
        print_attr!(self, disp: directory("directory"), f, first_char);
        print_attr!(self, disp: all_signature("all signature"), f, first_char);
        print_attr!(self, disp_list: signatures("signatures"), f, first_char);
        print_attr!(self, disp_list: ignore("ignore"), f, first_char);
        print_attr!(self, disp_list: whitelist("whitelist"), f, first_char);
        print_attr!(self, disp: destination("destination"), f, first_char);

        writeln!(f, "{}   └ paths", first_char,)?;
        for (pos, path) in self.paths.iter().chain(self.files.iter()).with_position() {
            match pos {
                itertools::Position::First | itertools::Position::Middle => {
                    writeln!(f, "{}     ├ path: {}", first_char, path)?
                }
                itertools::Position::Last | itertools::Position::Only => {
                    write!(f, "{}     └ path: {}", first_char, path)?
                }
            }
        }
        Ok(())
    }
}

#[derive(Debug, Deserialize)]
struct ProfileList {
    profiles: HashMap<String, Profile>,
}

impl ProfileList {
    fn get_similar_profile(&self, original_profile_name: &str) -> Option<&str> {
        // Get the more similar name base on jaro winkler
        let max = self
            .profiles
            .keys()
            .map(|profile_name| {
                let sim = strsim::jaro_winkler(original_profile_name, profile_name);
                (sim, profile_name)
            })
            .max_by(|(sim_1, _), (sim_2, _)| {
                sim_1.partial_cmp(sim_2).unwrap_or(std::cmp::Ordering::Less)
            });

        // If something similar found, use it
        if let Some((sim, profile_name)) = max {
            if sim > 0.5 {
                Some(profile_name)
            } else {
                None
            }
        } else {
            None
        }
    }

    fn add_from_stdin(&mut self) -> Result<()> {
        // Add profile list from stdin
        if !stdin().is_terminal() {
            let mut stdin_str = String::new();
            if stdin().read_to_string(&mut stdin_str)? != 0 {
                let stdin_profile_list: ProfileList = toml::from_str(&stdin_str)?;
                self.profiles
                    .extend(stdin_profile_list.profiles.into_iter());
            }
        }

        Ok(())
    }
}

pub fn print_profiles() -> Result<()> {
    let mut profile_list: ProfileList =
        toml::from_str(&fs::read_to_string("smc.toml").with_context(|| "No config file")?)
            .context("Invalid config file")?;
    profile_list.add_from_stdin()?;
    println!("{}", profile_list);
    Ok(())
}

impl Display for ProfileList {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "profiles")?;
        for (profile_pos, (name, profile)) in self.profiles.iter().with_position() {
            match profile_pos {
                itertools::Position::Middle => {
                    writeln!(f, "├── {}", name)?;
                    writeln!(f, "{:+}", profile)?;
                }
                itertools::Position::First => {
                    writeln!(f, "├── {}", name)?;
                    writeln!(f, "{:+}", profile)?;
                }
                itertools::Position::Last => {
                    writeln!(f, "└── {}", name)?;
                    write!(f, "{}", profile)?;
                }
                itertools::Position::Only => {
                    writeln!(f, "└── {}", name)?;
                    write!(f, "{}", profile)?;
                }
            }
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct RecordConfig {
    name: String,
    paths: Vec<RelativePathBuf>,
    hidden: bool,
    gitignore: bool,
    ignore: Vec<String>,
    whitelist: Vec<String>,
    directory: Option<RelativePathBuf>,
    files: Vec<RelativePathBuf>,
    destination: Option<RelativePathBuf>,
}

impl From<(&str, Profile)> for RecordConfig {
    fn from((profile_name, profile): (&str, Profile)) -> Self {
        RecordConfig {
            name: profile_name.to_string(),
            paths: profile.paths,
            hidden: profile.hidden.unwrap(),
            gitignore: profile.gitignore.unwrap(),
            directory: profile.directory,
            files: profile.files,
            ignore: profile.ignore,
            whitelist: profile.whitelist,
            destination: profile.destination,
        }
    }
}

#[derive(Debug)]
pub struct Config {
    pub container: ContainerType,
    pub output: PathBuf,
    pub save_dir: String,
    pub compression: CompressionType,
    pub compression_level: Option<i32>,
    pub signatures: Vec<Signature>,
    pub all_signature: Option<Signature>,
    pub signature_generate_config: signature::GenerateConfig,
    pub metadata: bool,
    pub records: Vec<RecordConfig>,
}

impl Config {
    fn from_profiles<'a>(iter: impl IntoIterator<Item = (&'a str, Profile)>) -> Result<Self> {
        let mut profile_iter = iter.into_iter();
        let (first_profile_name, first_profile) =
            profile_iter.next().ok_or_else(|| anyhow!("No profile"))?;

        let container = first_profile.container.unwrap();
        let compression = first_profile.compression.unwrap();
        let compression_level = first_profile.compression_level;
        let output = first_profile.output.as_ref().unwrap().clone();
        let signatures = first_profile.signatures.clone();
        let all_signature = first_profile.all_signature;
        let metadata = first_profile.metadata.unwrap();
        let save_dir = first_profile.save_dir.as_ref().unwrap().clone();
        let records = vec![RecordConfig::from((first_profile_name, first_profile))];

        let mut config = Config {
            container,
            output,
            save_dir,
            compression,
            compression_level,
            signatures,
            all_signature,
            signature_generate_config: signature::GenerateConfig::default(),
            metadata,
            records,
        };

        config
            .records
            .extend(profile_iter.map(|profile| profile.into()));

        Ok(config)
    }

    fn add_cmd(&mut self, matches: &ArgMatches) -> Result<()> {
        // Container
        if let Some(container) = matches.get_one::<String>("container") {
            self.container = ContainerType::from_str(container).unwrap();
        }

        // Add metadata
        if matches.get_flag("metadata") {
            self.metadata = true;
        }

        // Add compression
        if let Some(compression_level) = matches.get_one("compression_level") {
            self.compression_level = Some(*compression_level);
        }
        if let Some(compression) = matches.get_one::<String>("compression") {
            self.compression = CompressionType::from_str(compression).unwrap();
        }

        // Add output
        if let Some(output_dir) = matches.get_one::<PathBuf>("output") {
            self.output = output_dir.clone()
        }

        // Add save dir
        if let Some(save_dir) = matches.get_one::<String>("save_dir") {
            self.save_dir = save_dir.clone()
        }

        // Add paths
        if let Some(paths) = matches.get_raw("paths") {
            self.records[0].paths.extend(
                paths.map(|f| RelativePathBuf::from(Path::new(f).to_slash_lossy().as_ref())),
            );
        }

        // Add files
        if let Some(files) = matches.get_raw("files") {
            self.records[0].files.extend(
                files.map(|f| RelativePathBuf::from(Path::new(f).to_slash_lossy().as_ref())),
            );
        }

        // Add signatures
        if let Some(files) = matches.get_many::<String>("signatures") {
            self.signatures
                .extend(files.map(|f| Signature::from_str(f).unwrap()));
        }

        // Add all signatures
        if let Some(signature) = matches.get_one::<String>("signatures") {
            self.all_signature = Some(Signature::from_str(signature).unwrap());
        }

        // Add minisign config
        if let Some(path) = matches.get_one::<PathBuf>("minisign_secret_key_path") {
            self.signature_generate_config.minisign_secret_key_path = Some(path.clone());
        }

        // Add openpgp config
        if let Some(path) = matches.get_one::<PathBuf>("openpgp_secret_key_path") {
            self.signature_generate_config.openpgp_secret_key_path = Some(path.clone());
        }

        Ok(())
    }

    fn expand(&self, info: &crate::config::Info, input: impl AsRef<Path>) -> PathBuf {
        let expanded = shellexpand::path::env_with_context_no_errors(input.as_ref(), |s| match s {
            "TIME" => Some(Cow::Owned(PathBuf::from(
                info.local_date_time.format("%Y-%m-%d_%H-%M-%S").to_string(),
            ))),
            "CURRENT_DIR" => Some(Cow::Borrowed(Path::new(&info.current_dir))),
            "SAVE_DIR" => Some(Cow::Borrowed(Path::new(&self.save_dir))),
            "EXT" => Some(Cow::Borrowed(Path::new(
                self.container.extension(self.compression),
            ))),
            "GIT_FULL_COMMIT_HASH" => info
                .current_commit
                .as_ref()
                .map(|commit| Cow::Borrowed(Path::new(&commit.full_id))),
            "GIT_COMMIT_HASH" => info
                .current_commit
                .as_ref()
                .map(|commit| Cow::Borrowed(Path::new(&commit.id))),
            "GIT_BRANCH" => info
                .git_branch
                .as_ref()
                .map(|git_branch| Cow::Borrowed(Path::new(git_branch))),
            "GIT_TAG" => info
                .current_commit
                .as_ref()
                .map(|commit| commit.tag.as_ref().map(|tag| Cow::Borrowed(Path::new(tag))))
                .flatten(),
            "TMP" => Some(Cow::Owned(std::env::temp_dir())),
            _ => {
                warn!("Unknown variable {}", s);
                None
            }
        });

        debug!(
            "Expand from `{}` to `{}`",
            input.as_ref().display(),
            expanded.display()
        );

        expanded.to_path_buf()
    }

    fn set_output(&mut self, info: &crate::config::Info) -> Result<()> {
        // Exand save dir
        self.save_dir = self
            .expand(info, &self.save_dir)
            .to_string_lossy()
            .to_string();

        // Create output filename
        if self.output.is_dir() {
            self.output = self.output.join("${CURRENT_DIR}_${TIME}${EXT}");
        }

        // Expand output
        self.output = self.expand(info, &self.output);

        Ok(())
    }

    pub fn new(matches: &ArgMatches, info: &crate::config::Info) -> Result<Self> {
        let mut config = if matches.contains_id("profiles") {
            // Read profile list from smc.toml
            let mut profile_list: ProfileList =
                toml::from_str(&fs::read_to_string("smc.toml").with_context(|| "No config file")?)
                    .context("Invalid config file")?;

            // Add profile list from stdin
            profile_list.add_from_stdin()?;

            Config::from_profiles(matches.get_many::<String>("profiles").unwrap().filter_map(
                |profile_name| {
                    let profile = profile_list.profiles.remove(profile_name);
                    if profile.is_none() {
                        warn!("Profile `{}` not found", profile_name);
                        if let Some(profile_similar) =
                            profile_list.get_similar_profile(profile_name)
                        {
                            info!("Did you mean `{}` ?", profile_similar);
                        }
                    }
                    profile.map(|profile| {
                        (
                            profile_name.as_str(),
                            profile.add_cmd(matches).unwrap().or_default(),
                        )
                    })
                },
            ))?
        } else {
            Config::from_profiles(std::iter::once((
                "default",
                Profile::default().add_cmd(matches)?,
            )))
            .unwrap()
        };

        // Add commande line
        config.add_cmd(matches)?;

        // Set output
        config.set_output(info)?;

        Ok(config)
    }
}

pub fn save(
    multi_progress_bar: MultiProgress,
    mut container: impl crate::container::Container,
    config: Config,
    info: &Info,
) -> Result<()> {
    debug!(
        "Run with config {:?} on {} to {}",
        config,
        &config.save_dir,
        config.output.display()
    );

    let saved_current_dir = current_dir()?;

    // Multi signature
    let mut multi_signature = config
        .all_signature
        .map(|sig| signature::Multi::new(sig, &config.output, &config.save_dir))
        .transpose()?;

    let record_progress_bar = multi_progress_bar.add(
        ProgressBar::new(config.records.len() as u64).with_style(
            ProgressStyle::with_template(
                "  [{elapsed:6}] Record {prefix:17} {pos:3} / {len:3} {wide_bar}                                  ",
            )
            .unwrap(),
        ),
    );

    info!("Save the code in {}", config.output.display());
    for record in config.records.into_iter() {
        info!("Record {}", &record.name);
        record_progress_bar.set_prefix(record.name.clone());
        record_progress_bar.inc(1);

        let output_can = config.output.canonicalize()?;

        // Set current dir
        if let Some(dir) = &record.directory {
            set_current_dir(dir.to_logical_path(&saved_current_dir))?;
        }

        // Get files in paths
        let (mut files, mut total_size) = if !record.paths.is_empty() {
            // Create override
            let mut walker_override_builder =
                ignore::overrides::OverrideBuilder::new(current_dir()?);
            for whitelist in &record.whitelist {
                walker_override_builder.add(whitelist)?;
            }
            for ignore in &record.ignore {
                walker_override_builder.add(&format!("!{}", ignore))?;
            }

            // Create walker
            let mut walker = WalkBuilder::new(&record.paths[0].to_logical_path("."));
            walker
                .same_file_system(true)
                .skip_stdout(true)
                .follow_links(false)
                .hidden(!record.hidden)
                .git_ignore(record.gitignore)
                .overrides(walker_override_builder.build()?)
                // Filter output file
                .filter_entry(move |entry| {
                    if entry.path().file_name().unwrap() == output_can.file_name().unwrap() {
                        if let Ok(can_entry_path) = entry.path().canonicalize() {
                            can_entry_path != output_can
                        } else {
                            true
                        }
                    } else {
                        true
                    }
                });
            for path in record.paths.iter().skip(1) {
                walker.add(path.to_logical_path("."));
            }

            // Progress bar
            let walk_progress_bar = multi_progress_bar.add(
                ProgressBar::new_spinner().with_style(
                    ProgressStyle::with_template(
                        "{spinner} [{elapsed:6}] Current file {human_pos:10} : Speed {per_sec:10}",
                    )
                    .unwrap(),
                ),
            );

            // Get files
            info!("Get files list");
            let mut total_size = 0;
            let files: Vec<_> = walker
                .build()
                .progress_with(walk_progress_bar)
                .filter_map(|result| match result {
                    Ok(entry) => entry.path().metadata().ok().map(|file_metadata| {
                        let size = file_metadata.len();
                        total_size += size;
                        (
                            size,
                            RelativePath::new(&entry.path().to_slash_lossy())
                                .to_relative_path_buf(),
                        )
                    }),
                    Err(err) => {
                        warn!("Cannot read path: {}", err);
                        None
                    }
                })
                .collect();

            (files, total_size)
        } else {
            (Vec::new(), 0)
        };

        // Add single files
        files.extend(record.files.into_iter().filter_map(|relative_path| {
            relative_path
                .to_logical_path(".")
                .metadata()
                .ok()
                .map(|file_metadata| {
                    let size = file_metadata.len();
                    total_size += size;
                    (size, relative_path)
                })
        }));

        // Progress bars
        let save_index_progress_bar = multi_progress_bar.add(ProgressBar::new(files.len() as u64).with_style(
            ProgressStyle::with_template(
                "{spinner} [{elapsed:6}] Current file {human_pos:10}/{human_len:10} {wide_bar} Remaining {eta:6} Speed {per_sec:10}",
            )
            .unwrap(),
        ));
        let save_size_progress_bar = multi_progress_bar.add(ProgressBar::new(total_size).with_style(
            ProgressStyle::with_template(
                "{spinner} [{elapsed:6}] Current file {bytes:10}/{total_bytes:10} {wide_bar}                 Speed {bytes_per_sec}",
            )
            .unwrap(),
        ));

        // Save files
        info!("Save");
        for (size, path) in files.into_iter().progress_with(save_index_progress_bar) {
            debug!("Save file '{}'", &path);
            if let Err(e) = container.add_path(&path, record.destination.as_deref()) {
                warn!("Cannot add '{}': {}", &path, e);
            }

            // Add signature
            if let Some(multi_signature) = multi_signature.as_mut() {
                if let Err(e) = multi_signature.add_path(&path) {
                    warn!("Cannot generate signature for '{}': {}", path, e);
                }
            }

            save_size_progress_bar.inc(size);
        }

        // Reset current dir
        if record.directory.is_some() {
            set_current_dir(&saved_current_dir)?;
        }
    }

    // Delete record progress bar
    drop(record_progress_bar);

    // Save metadata
    if config.metadata {
        info!("Save metadata");
        let info_yaml = serde_yaml::to_string(info)?;
        container.add_file(
            RelativePath::new("./.smc/metadata.yaml"),
            info_yaml.as_bytes(),
        )?;
    }

    // Finish container and signatures
    container.finish()?;
    if let Some(multi_signature) = multi_signature.take() {
        multi_signature.finalize()?;
    }

    // Add signatures
    let sig_progress_bar = multi_progress_bar.add(
        ProgressBar::new(config.signatures.len() as u64).with_style(
            ProgressStyle::with_template(
                "{spinner} [{elapsed:6}] Hash {human_pos:2}/{human_len:2} {wide_bar} Remaining {eta:6} Speed {per_sec:10}",
            )
            .unwrap(),
        ),
    );

    for signature in config
        .signatures
        .into_iter()
        .progress_with(sig_progress_bar)
    {
        signature.generate_signature(&config.output, &config.signature_generate_config)?;
    }

    Ok(())
}
