use crate::backup::Config;
use anyhow::Result;
use bzip2::write::BzEncoder;
use bzip3::write::Bz3Encoder;
use chrono::{Datelike, NaiveDateTime, TimeZone, Timelike};
use filetime::{set_file_times, FileTime};
use flate2::write::GzEncoder;
use flate2::GzBuilder;
use log::debug;
use relative_path::RelativePath;
use sevenz_rust::{SevenZArchiveEntry, SevenZWriter};
use std::io::Write;
use std::{fs, io};
use std::{
    fs::File,
    io::{BufReader, BufWriter},
    path::PathBuf,
    time::UNIX_EPOCH,
};
use xz2::write::XzEncoder;
use zip::ZipWriter;

pub trait Container {
    fn add_path(
        &mut self,
        filename: &RelativePath,
        destination: Option<&RelativePath>,
    ) -> Result<()>;
    fn add_file(&mut self, filename: &RelativePath, data: &[u8]) -> Result<()>;
    fn finish(self) -> Result<()>
    where
        Self: Sized,
    {
        Ok(())
    }
}

fn resolve_path(
    save_dir: &str,
    filename: &RelativePath,
    destination: Option<&RelativePath>,
) -> String {
    if let Some(destination) = destination {
        RelativePath::new(save_dir)
            .join_normalized(destination)
            .join_normalized(filename.file_name().unwrap_or_default())
            .into_string()
    } else {
        RelativePath::new(save_dir)
            .join_normalized(filename)
            .into_string()
    }
}

pub struct Zip {
    writer: ZipWriter<BufWriter<File>>,
    option: zip::write::FileOptions,
    save_dir: String,
}

impl Zip {
    pub fn new(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let zip = ZipWriter::new(file);
        let zip_options = zip::write::FileOptions::default()
            .compression_method(config.compression.try_into()?)
            .compression_level(config.compression_level);
        Ok(Zip {
            writer: zip,
            option: zip_options,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl Container for Zip {
    fn add_path(
        &mut self,
        filename: &RelativePath,
        destination: Option<&RelativePath>,
    ) -> Result<()> {
        let std_filename = filename.to_logical_path(".");

        // Set date/time
        let metadata = fs::metadata(&std_filename)?;
        let file_type = metadata.file_type();
        let mut options = self.option;

        if let Ok(sys_time) = metadata.modified() {
            let timestamp = sys_time.duration_since(UNIX_EPOCH)?;
            let utc_date_time = NaiveDateTime::from_timestamp_opt(
                timestamp.as_secs() as i64,
                timestamp.subsec_nanos(),
            )
            .unwrap();
            let local_date_time = chrono::Local.from_utc_datetime(&utc_date_time);
            let zip_date_time = zip::DateTime::from_date_and_time(
                local_date_time.year() as u16,
                local_date_time.month() as u8,
                local_date_time.day() as u8,
                local_date_time.hour() as u8,
                local_date_time.minute() as u8,
                local_date_time.second() as u8,
            )
            .unwrap();
            options = options.last_modified_time(zip_date_time);
        }

        // Set path
        let path = resolve_path(&self.save_dir, &filename, destination);

        debug!("Add '{}' in zip", path);
        if file_type.is_file() {
            self.writer.start_file(path, options)?;
            let mut file = BufReader::new(File::open(&std_filename)?);
            std::io::copy(&mut file, &mut self.writer)?;
        } else if file_type.is_dir() && destination.is_none() {
            self.writer.add_directory(path, options)?;
        }

        Ok(())
    }

    fn add_file(&mut self, filename: &RelativePath, mut data: &[u8]) -> Result<()> {
        let path = resolve_path(&self.save_dir, &filename, None);
        debug!("Add '{}' in zip", path);
        self.writer.start_file(path, self.option)?;
        std::io::copy(&mut data, &mut self.writer)?;
        Ok(())
    }

    fn finish(mut self) -> Result<()> {
        self.writer.finish()?;
        Ok(())
    }
}

pub struct SevenZip {
    writer: SevenZWriter<BufWriter<File>>,
    save_dir: String,
}

impl SevenZip {
    pub fn new(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let writer = SevenZWriter::new(file)?;
        Ok(SevenZip {
            writer,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl Container for SevenZip {
    fn add_path(
        &mut self,
        filename: &RelativePath,
        destination: Option<&RelativePath>,
    ) -> Result<()> {
        let std_filename = filename.to_logical_path(".");

        if std_filename.is_file() {
            // Set path
            let path = resolve_path(&self.save_dir, &filename, destination);

            let file = BufReader::new(File::open(&std_filename)?);
            self.writer.push_archive_entry(
                SevenZArchiveEntry::from_path(&std_filename, path),
                Some(file),
            )?;
        }
        Ok(())
    }

    fn add_file(&mut self, filename: &RelativePath, data: &[u8]) -> Result<()> {
        let path = resolve_path(&self.save_dir, &filename, None);
        let std_filename = filename.to_logical_path(".");
        self.writer.push_archive_entry(
            SevenZArchiveEntry::from_path(&std_filename, path),
            Some(data),
        )?;
        Ok(())
    }

    fn finish(self) -> Result<()> {
        self.writer.finish()?;
        Ok(())
    }
}

pub struct Plain {
    path: PathBuf,
}

impl Plain {
    pub fn new(config: &Config) -> Result<Self> {
        let path = config.output.join(&config.save_dir);
        fs::create_dir_all(&path)?;
        Ok(Plain { path })
    }
}

impl Container for Plain {
    fn add_path(
        &mut self,
        filename: &RelativePath,
        destination: Option<&RelativePath>,
    ) -> Result<()> {
        let path = if let Some(destination) = destination {
            let path = destination.to_logical_path(&self.path);
            if let Some(file_name) = filename.file_name() {
                path.join(file_name)
            } else {
                path
            }
        } else {
            filename.to_logical_path(&self.path)
        };

        let std_filename = filename.to_logical_path(".").canonicalize()?;

        if std_filename.is_file() {
            // Create parent if needed
            if let Some(parent_dir) = path.parent() {
                if !parent_dir.exists() {
                    fs::create_dir_all(parent_dir)?;
                }
            }

            match reflink_copy::reflink_or_copy(&std_filename, &path)? {
                Some(_) => (),
                None => {
                    debug!(
                        "Reflink from `{}` to `{}`",
                        std_filename.display(),
                        path.display()
                    );
                }
            }
        } else if std_filename.is_dir() && !path.exists() && destination.is_none() {
            debug!("Create dir '{}'", path.display());
            fs::create_dir(&path)?;
        } else {
            return Ok(());
        };

        // Copy times
        if let Ok(src_meta) = fs::metadata(&std_filename) {
            let mtime = FileTime::from_last_modification_time(&src_meta);
            let atime = FileTime::from_last_access_time(&src_meta);
            set_file_times(&path, atime, mtime)?;
        }

        Ok(())
    }

    fn add_file(&mut self, filename: &RelativePath, mut data: &[u8]) -> Result<()> {
        let path = filename.to_logical_path(&self.path);

        // Create parent if needed
        if let Some(parent_dir) = path.parent() {
            if !parent_dir.exists() {
                fs::create_dir_all(parent_dir)?;
            }
        }

        // Write file
        let mut writer = BufWriter::new(File::create(&path)?);
        io::copy(&mut data, &mut writer)?;

        Ok(())
    }
}

pub struct Tar<W: Write> {
    writer: tar::Builder<W>,
    save_dir: String,
}

impl Tar<BufWriter<std::fs::File>> {
    pub fn new(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let writer = tar::Builder::new(file);
        Ok(Tar {
            writer,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl Tar<GzEncoder<BufWriter<std::fs::File>>> {
    pub fn new_gzip(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let gz_enc = GzBuilder::new()
            .filename(
                config
                    .output
                    .file_name()
                    .unwrap()
                    .to_string_lossy()
                    .to_string(),
            )
            .write(
                file,
                flate2::Compression::new(config.compression_level.unwrap_or(6) as u32),
            );
        let writer = tar::Builder::new(gz_enc);
        Ok(Tar {
            writer,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl Tar<zstd::stream::AutoFinishEncoder<'_, BufWriter<File>>> {
    pub fn new_zstd(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let zstd_enc =
            zstd::stream::write::Encoder::new(file, config.compression_level.unwrap_or(0))?
                .auto_finish();
        let writer = tar::Builder::new(zstd_enc);
        Ok(Tar {
            writer,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl Tar<BzEncoder<BufWriter<File>>> {
    pub fn new_bz2(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let bz2_enc = BzEncoder::new(
            file,
            bzip2::Compression::new(config.compression_level.unwrap_or(6) as u32),
        );
        let writer = tar::Builder::new(bz2_enc);
        Ok(Tar {
            writer,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl Tar<Bz3Encoder<BufWriter<File>>> {
    pub fn new_bz3(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let bz3_enc = Bz3Encoder::new(file, 16 * 1024 * 1024)?;
        let writer = tar::Builder::new(bz3_enc);
        Ok(Tar {
            writer,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl Tar<XzEncoder<BufWriter<File>>> {
    pub fn new_xz(config: &Config) -> Result<Self> {
        let file = BufWriter::new(File::create(&config.output)?);
        let bz2_enc = XzEncoder::new(file, config.compression_level.unwrap_or(6) as u32);
        let writer = tar::Builder::new(bz2_enc);
        Ok(Tar {
            writer,
            save_dir: config.save_dir.clone(),
        })
    }
}

impl<W: Write> Container for Tar<W> {
    fn add_path(
        &mut self,
        filename: &RelativePath,
        destination: Option<&RelativePath>,
    ) -> Result<()> {
        let std_filename = filename.to_logical_path(".");

        // Set path
        let path = resolve_path(&self.save_dir, &filename, destination);

        if !path.is_empty() {
            if std_filename.is_file() {
                self.writer.append_path_with_name(std_filename, path)?;
            } else if std_filename.is_dir() && destination.is_none() {
                self.writer.append_dir(path, std_filename)?;
            }
        }

        Ok(())
    }

    fn add_file(&mut self, filename: &RelativePath, data: &[u8]) -> Result<()> {
        let path = resolve_path(&self.save_dir, &filename, None);

        // Create header
        let mut header = tar::Header::new_gnu();
        header.set_path(path)?;
        header.set_size(data.len() as u64);
        header.set_mode(0o666);
        header.set_cksum();

        self.writer.append(&header, data)?;

        Ok(())
    }

    fn finish(mut self) -> Result<()> {
        self.writer.finish()?;
        Ok(())
    }
}
