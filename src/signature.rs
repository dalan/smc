use anyhow::{anyhow, bail, Context, Result};
use log::{debug, info};
use relative_path::RelativePath;
use sequoia_openpgp::parse::Parse;
use serde::Deserialize;
use std::{
    fs::{self, File},
    io::{self, BufReader, BufWriter, Write},
    path::{Path, PathBuf},
};
use strum_macros::{Display, EnumString, VariantNames};

#[derive(Clone, Debug, Deserialize, Default)]
pub struct GenerateConfig {
    pub minisign_secret_key_path: Option<PathBuf>,
    pub minisign_secret_key_password: Option<String>,
    pub openpgp_secret_key_path: Option<PathBuf>,
    pub openpgp_secret_key_password: Option<String>,
}

#[derive(Copy, Clone, Debug, EnumString, Deserialize, VariantNames, Display)]
#[strum(ascii_case_insensitive)]
pub enum Signature {
    Blake3,
    Sha256,
    Sha384,
    Sha512,
    Sha3_256,
    Sha3_384,
    Sha3_512,
    Minisign,
    OpenPGP,
}

impl Signature {
    fn extension(&self) -> &'static str {
        match self {
            Signature::Blake3 => ".b3sum",
            Signature::Sha256 => ".sha256sum",
            Signature::Sha384 => ".sha384sum",
            Signature::Sha512 => ".sha512sum",
            Signature::Sha3_256 => ".sha3_256sum",
            Signature::Sha3_384 => ".sha3_384sum",
            Signature::Sha3_512 => ".sha3_512sum",
            Signature::Minisign => ".minisig",
            Signature::OpenPGP => ".sig",
        }
    }

    fn filename(&self, path: &Path) -> PathBuf {
        let mut output_filename = path.to_path_buf();
        output_filename.set_file_name(format!(
            "{}{}",
            path.file_name().unwrap().to_string_lossy(),
            self.extension()
        ));
        output_filename
    }

    pub fn generate_signature(self, path: &Path, config: &GenerateConfig) -> Result<()> {
        let sig_filename = self.filename(path);

        match self {
            Signature::Blake3 => self::blake3::generate(path, &sig_filename),
            Signature::Minisign => self::minisgn::generate(
                path,
                &sig_filename,
                config.minisign_secret_key_path.as_deref(),
                config.minisign_secret_key_password.as_deref(),
            ),
            Signature::OpenPGP => openpgp::generate(
                path,
                &sig_filename,
                config.openpgp_secret_key_path.as_deref(),
                config.openpgp_secret_key_password.as_deref(),
            ),
            _ => hash::generate(self, path, &sig_filename),
        }
    }
}

pub struct Multi {
    sig: Signature,
    file: BufWriter<File>,
    save_dir: String,
}

impl Multi {
    pub fn new(sig: Signature, path: &Path, save_dir: impl Into<String>) -> Result<Self> {
        let mut sig_filename = sig.filename(path);
        sig_filename.set_file_name(format!(
            "{}_all",
            sig_filename.file_name().unwrap().to_string_lossy()
        ));
        Ok(Multi {
            sig,
            file: BufWriter::new(File::create(sig_filename)?),
            save_dir: save_dir.into(),
        })
    }

    pub fn add_path(&mut self, rel_path: &RelativePath) -> Result<()> {
        let filename = rel_path.to_logical_path(".");
        let save_filename = rel_path.to_logical_path(&self.save_dir);
        let save_filename_str = save_filename.to_string_lossy();
        if filename.is_file() {
            debug!("Add signature for `{}`", filename.display());
            writeln!(
                self.file,
                "{} *{}",
                hash::hash(self.sig, &filename)?,
                save_filename_str
            )?;
        }
        Ok(())
    }

    pub fn finalize(mut self) -> Result<()> {
        self.file.flush()?;
        Ok(())
    }
}

mod hash {
    use super::*;

    fn int_hash<D: digest::Digest + Write>(path: &Path) -> Result<String> {
        let mut file = BufReader::new(File::open(path)?);
        let mut hasher = D::new();
        std::io::copy(&mut file, &mut hasher)?;
        let output = hasher.finalize();
        Ok(hex::encode(output))
    }

    fn space_format(sig: Signature) -> &'static str {
        match sig {
            Signature::Blake3 => "  ",
            Signature::Sha256 => " *",
            Signature::Sha384 => " *",
            Signature::Sha512 => " *",
            Signature::Sha3_256 => " ",
            Signature::Sha3_384 => " ",
            Signature::Sha3_512 => " ",
            Signature::Minisign => panic!(),
            Signature::OpenPGP => panic!(),
        }
    }

    pub fn hash(sig: Signature, path: &Path) -> Result<String> {
        match sig {
            Signature::Blake3 => int_hash::<::blake3::Hasher>(path),
            Signature::Sha256 => int_hash::<sha2::Sha256>(path),
            Signature::Sha384 => int_hash::<sha2::Sha384>(path),
            Signature::Sha512 => int_hash::<sha2::Sha512>(path),
            Signature::Sha3_256 => int_hash::<sha3::Sha3_256>(path),
            Signature::Sha3_384 => int_hash::<sha3::Sha3_384>(path),
            Signature::Sha3_512 => int_hash::<sha3::Sha3_512>(path),
            Signature::Minisign => panic!(),
            Signature::OpenPGP => panic!(),
        }
    }

    pub fn generate(sig: Signature, path: &Path, sig_filename: &Path) -> Result<()> {
        info!("Generate {}", sig);

        fs::write(
            sig_filename,
            format!(
                "{}{}{}",
                hash(sig, path)?,
                space_format(sig),
                path.file_name().unwrap().to_string_lossy()
            ),
        )?;

        Ok(())
    }
}

mod blake3 {
    use super::*;
    use ::blake3::Hasher;

    pub fn generate(path: &Path, sig_filename: &Path) -> Result<()> {
        info!("Generate Blake3");

        let mut hasher = Hasher::new();
        hasher.update_mmap_rayon(path)?;

        fs::write(
            sig_filename,
            format!(
                "{}  {}",
                hasher.finalize(),
                path.file_name().unwrap().to_string_lossy()
            ),
        )?;

        Ok(())
    }
}

mod minisgn {
    use super::*;

    fn get_sk_path(explicit_path: Option<&Path>) -> Result<PathBuf> {
        const DEFAULT_FILE_NAME: &'static str = minisign::SIG_DEFAULT_SKFILE;
        match explicit_path {
            Some(explicit_path) => Ok(PathBuf::from(explicit_path)),
            None => match std::env::var(minisign::SIG_DEFAULT_CONFIG_DIR_ENV_VAR) {
                Ok(env_path) => {
                    let mut complete_path = PathBuf::from(env_path);
                    complete_path.push(DEFAULT_FILE_NAME);
                    Ok(complete_path)
                }
                Err(_) => {
                    // Get home dir
                    let base_dire = directories::BaseDirs::new().ok_or_else(|| {
                        anyhow!("Cannot find home dir for retreiving minisign private key")
                    })?;
                    let home_path = base_dire.home_dir();

                    // Test default rsign
                    let mut rsign_path = home_path.to_path_buf();
                    rsign_path.push(minisign::SIG_DEFAULT_CONFIG_DIR);
                    rsign_path.push(DEFAULT_FILE_NAME);

                    if rsign_path.exists() {
                        Ok(rsign_path)
                    } else {
                        // Test default minisgn
                        let mut minisign_path = home_path.to_path_buf();
                        minisign_path.push(".minisign");
                        minisign_path.push("minisign.key");

                        if minisign_path.exists() {
                            Ok(minisign_path)
                        } else {
                            bail!("Cannot find minisgn private key");
                        }
                    }
                }
            },
        }
    }

    pub fn generate(
        path: &Path,
        sig_filename: &Path,
        secret_key_path: Option<&Path>,
        secret_key_password: Option<&str>,
    ) -> Result<()> {
        info!("Sign with Minisgn");

        // Get secret key
        let secret_key_path = get_sk_path(secret_key_path)?;
        let sk_box = minisign::SecretKeyBox::from_string(&fs::read_to_string(secret_key_path)?)?;
        let sk = sk_box.into_secret_key(secret_key_password.map(|s| s.to_string()))?;

        // Generate signature
        let file = BufReader::new(File::open(path)?);
        let signature = minisign::sign(None, &sk, file, None, None)?;

        // Write signature
        fs::write(sig_filename, signature.to_string())?;

        Ok(())
    }
}

mod openpgp {
    use super::*;
    use log::warn;
    use sequoia_openpgp::{
        armor,
        cert::{amalgamation::ValidAmalgamation, CertParser},
        crypto::{KeyPair, Password},
        packet::{key::SecretKeyMaterial, signature::SignatureBuilder},
        policy::{Policy, StandardPolicy},
        serialize::stream::{Armorer, Message, Signer},
        types::{KeyFlags, RevocationStatus, SignatureType},
        Cert,
    };

    fn get_sk_path(explicit_path: Option<&Path>) -> Result<PathBuf> {
        const DEFAULT_FILE_NAME: &'static str = "keys.pgp";
        match explicit_path {
            Some(explicit_path) => Ok(PathBuf::from(explicit_path)),
            None => {
                // Get home dir
                let base_dire = directories::BaseDirs::new().ok_or_else(|| {
                    anyhow!("Cannot find home dir for retreiving OpenPGP private key")
                })?;
                let home_path = base_dire.home_dir();

                // Test default rsign
                let mut rsign_path = home_path.to_path_buf();
                rsign_path.push(".gnupg");
                rsign_path.push(DEFAULT_FILE_NAME);

                if rsign_path.exists() {
                    Ok(rsign_path)
                } else {
                    bail!("Cannot find OpenPGP private key");
                }
            }
        }
    }

    /// Loads one or more certs from every given file.
    fn load_certs(file: &Path) -> Result<Vec<Cert>> {
        let mut certs = vec![];
        for maybe_cert in CertParser::from_file(file)
            .context(format!("Failed to load certs from file {:?}", file))?
        {
            certs.push(maybe_cert.context(format!("A cert from file {:?} is bad", file))?);
        }
        Ok(certs)
    }

    /// Returns suitable signing keys from a given list of Certs.
    fn get_signing_keys(
        certs: &[Cert],
        policy: &impl Policy,
        secret_key_password: Option<&str>,
    ) -> Result<Vec<(KeyPair, Option<Password>)>> {
        let mut keys: Vec<(KeyPair, Option<Password>)> = vec![];
        for tsk in certs {
            debug!("Cert {}", tsk);

            // Check policy
            let vc = tsk.with_policy(policy, None)?;

            for ka in vc.keys().key_flags(KeyFlags::empty().set_signing()) {
                // Check key
                if ka.alive().is_err() {
                    warn!("Key {} not alive", ka.keyid());
                    continue;
                }
                if let RevocationStatus::Revoked(_revs) = ka.revocation_status() {
                    warn!("Key {} is revoked", ka.keyid());
                    continue;
                }

                let key = ka.key();

                debug!("Key {}", key);

                if let Some(secret) = key.optional_secret() {
                    let (unencrypted, password) = match secret {
                        SecretKeyMaterial::Encrypted(ref e) => {
                            // Try to re-use password for other key
                            match keys.iter().find_map(|(_, password)| {
                                password.as_ref().and_then(|p| {
                                    e.decrypt(key.pk_algo(), p).ok().map(|key| (key, p.clone()))
                                })
                            }) {
                                Some((unencrypted, password)) => (unencrypted, Some(password)),
                                None => {
                                    // Ask password
                                    let password = secret_key_password
                                        .unwrap_or(
                                            &rpassword::prompt_password(&format!(
                                                "Please enter the password to decrypt the key {}/{}: ",
                                                tsk.keyid(), key.keyid()
                                            ))
                                            .unwrap(),
                                        )
                                        .into();
                                    (
                                        e.decrypt(key.pk_algo(), &password)
                                            .map_err(|_| anyhow!("Incorrect password."))?,
                                        Some(password),
                                    )
                                }
                            }
                        }
                        SecretKeyMaterial::Unencrypted(ref u) => (u.clone(), None),
                    };

                    debug!("Sign with {}", key.keyid());
                    keys.push((KeyPair::new(key.clone(), unencrypted).unwrap(), password));
                }
            }
        }

        Ok(keys)
    }

    pub fn generate(
        path: &Path,
        sig_filename: &Path,
        secret_key_path: Option<&Path>,
        secret_key_password: Option<&str>,
    ) -> Result<()> {
        info!("Sign with OpenPGP");

        // Get secret cert
        let secrets = load_certs(&get_sk_path(secret_key_path)?)?;
        if secrets.is_empty() {
            bail!("No OpenPGP private key");
        }

        // Get keys
        let policy = StandardPolicy::new();
        let mut keys = get_signing_keys(&secrets, &policy, secret_key_password)?;
        if keys.is_empty() {
            return Err(anyhow::anyhow!("No signing keys found"));
        }

        // Create output file
        let mut output_file = BufWriter::new(File::create(sig_filename)?);

        // Create message
        let message = Armorer::new(Message::new(&mut output_file))
            .kind(armor::Kind::Signature)
            .build()?;

        // Create signer with the keys
        let builder = SignatureBuilder::new(SignatureType::Binary);
        let mut signer = Signer::with_template(message, keys.pop().unwrap().0, builder);
        for s in keys {
            signer = signer.add_signer(s.0);
        }
        signer = signer.detached();
        let mut signer = signer.build().context("Failed to create signer")?;

        // Finally, copy stdin to our writer stack to sign the data.
        io::copy(&mut BufReader::new(File::open(&path)?), &mut signer).context("Failed to sign")?;

        signer.finalize().context("Failed to sign")?;

        Ok(())
    }
}
