use anyhow::{bail, Result};
use clap::ArgMatches;
use config::CompressionType;
use config::Config;
use config::ContainerType;
use indicatif::MultiProgress;
use log::LevelFilter;
use log::{error, info};
use simplelog::ColorChoice;
use simplelog::ConfigBuilder;
use simplelog::TermLogger;
use simplelog::TerminalMode;

mod backup;
mod config;
mod container;
mod signature;

fn main() -> Result<()> {
    #[cfg(windows)]
    colored::control::set_virtual_terminal(true).unwrap();

    // Args
    let matches = config::create_app().get_matches();

    let multi_progress_bar = indicatif::MultiProgress::new();
    multi_progress_bar.set_move_cursor(false);

    // Init logs
    let level_filter = match matches.get_count("verbosity") {
        0 => LevelFilter::Info,
        1 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };
    indicatif_log_bridge::LogWrapper::new(
        multi_progress_bar.clone(),
        TermLogger::new(
            level_filter,
            ConfigBuilder::new()
                .set_time_offset_to_local()
                .unwrap()
                .build(),
            TerminalMode::Stderr,
            ColorChoice::Auto,
        ),
    )
    .try_init()
    .unwrap();

    if let Err(e) = run(multi_progress_bar, &matches) {
        error!("Fatal error occurred: {}", e);
        Err(e)
    } else {
        info!("Success");
        Ok(())
    }
}

fn run(multi_progress_bar: MultiProgress, matches: &ArgMatches) -> Result<()> {
    if matches.get_flag("profiles-list") {
        backup::print_profiles()?;
    } else {
        let config = Config::try_from(matches)?;

        match config.command {
            config::Command::Backup(backup_config) => {
                // Create zip file
                match backup_config.container {
                    ContainerType::None => backup::save(
                        multi_progress_bar,
                        container::Plain::new(&backup_config)?,
                        backup_config,
                        &config.info,
                    )?,
                    ContainerType::Zip => backup::save(
                        multi_progress_bar,
                        container::Zip::new(&backup_config)?,
                        backup_config,
                        &config.info,
                    )?,
                    ContainerType::SevenZip => backup::save(
                        multi_progress_bar,
                        container::SevenZip::new(&backup_config)?,
                        backup_config,
                        &config.info,
                    )?,
                    ContainerType::Tar => match backup_config.compression {
                        CompressionType::None => backup::save(
                            multi_progress_bar,
                            container::Tar::new(&backup_config)?,
                            backup_config,
                            &config.info,
                        )?,
                        CompressionType::Deflate => bail!("No deflate compression in tar"),
                        CompressionType::Bzip2 => backup::save(
                            multi_progress_bar,
                            container::Tar::new_bz2(&backup_config)?,
                            backup_config,
                            &config.info,
                        )?,
                        CompressionType::Bzip3 => backup::save(
                            multi_progress_bar,
                            container::Tar::new_bz3(&backup_config)?,
                            backup_config,
                            &config.info,
                        )?,
                        CompressionType::Zstd => backup::save(
                            multi_progress_bar,
                            container::Tar::new_zstd(&backup_config)?,
                            backup_config,
                            &config.info,
                        )?,
                        CompressionType::Gzip => backup::save(
                            multi_progress_bar,
                            container::Tar::new_gzip(&backup_config)?,
                            backup_config,
                            &config.info,
                        )?,
                        CompressionType::Xz => backup::save(
                            multi_progress_bar,
                            container::Tar::new_xz(&backup_config)?,
                            backup_config,
                            &config.info,
                        )?,
                    },
                };
            }
        }
    }
    Ok(())
}
